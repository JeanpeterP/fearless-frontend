import React from 'react';

export default function AttendeesList(props) {
    return (
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Names</th>
            <th>Conference</th>
            <th>Test</th>
            </tr>
        </thead>
        <tbody>
            {props.attendees.map(attendee => {
            return (
                <tr key={attendee.href}>
                <td>{ attendee.name }</td>
                <td>{ attendee.conference }</td>
                </tr>
            );
            })}
        </tbody>
        </table>
    );
}