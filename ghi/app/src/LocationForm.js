import React, {useState, useEffect} from 'react';

export default function LocationForm(props) {
    const [states, setStates] = useState([]);
    const [name, setName] = useState('');
    const [count, setCount] = useState(0);
    const [city, setCity] = useState('');
    const [state, setState] = useState('');

    const handleRoomCount = (event) => {
        const value = event.target.value;
        setCount(value);
    }

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        //event.target.value is the actual text that the user typed into the form
        // event.target is the user's input in the form for the location's name
        // target is the property that is the HTML tag that caused the event
        const value = event.target.value;
        setName(value);
    }

    const handleCity = (event) => {
        const value = event.target.value;
        setCity(value)
    }

    const handleState = (event) => {
        const value = event.target.value;
        setState(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.room_count = count;
        data.name = name;
        data.city = city;
        data.state = state;

        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('');
          setCount('');
          setCity('');
          setState('');
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8000/api/states/"

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states)
          } else {
            console.log("ERROR LOADING URL")
          }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (

        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleRoomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" value={count} />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCity} placeholder="City" required type="text" id="city" name="city" className="form-control" value={city} />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={handleState} required id="state" name="state" className="form-select" value={state}>
                  <option value="">Choose a state</option>
                  <>
                  {states.map(state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                    );
                    })}
                </>
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
