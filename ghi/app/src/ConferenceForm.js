import React, {useState, useEffect} from 'react';

export default function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [start, setStart] = useState('')
    const [end, setEnd] = useState('');
    const [description, setDesc] = useState('');
    const [presentation, setPresentation] = useState(0);
    const [attendees, setAttendees] = useState(0);
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartDate = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndDate = (event) => {
        const value = event.target.value;
        setEnd(value)
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDesc(value)
    }

    const handleMaxPresentations = (event) => {
        const value = event.target.value;
        setPresentation(value)
    }

    const handleMaxAttendees = (event) => {
        const value = event.target.value;
        setAttendees(value)
    }

    const handleLocation = (event) => {
        const value = event.target.value;
        setLocation(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {};

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = presentation;
        data.max_attendees = attendees;
        data.location = location;
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)

            setName('');
            setStart('');
            setEnd('');
            setDesc('');
            setPresentation('');
            setAttendees('');
            setLocation('');

        } else {
            console.log("ERROR LOADING URL RESPONSE")
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        } else {
            console.log("ERROR FETCHING URL")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartDate} placeholder="starts" required type="date" name="starts" id="starts" className="form-control" />
                    <label htmlFor="room_count">Start Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEndDate} placeholder="ends" required type="date" id="ends" name="ends" className="form-control" />
                    <label htmlFor="city">End Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleDescriptionChange} placeholder="description" required type="text" id="description" name="description" className="form-control" />
                    <label htmlFor="city">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxPresentations} placeholder="max_presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                    <label htmlFor="city">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxAttendees} placeholder="max_attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                    <label htmlFor="city">Maximum attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocation} required id="location" name="location" className="form-select">
                    <option  value="">Choose a location</option>
                    <>
                        {locations.map(location => {
                            return (
                                <option key={location.id} value={location.id}>
                                    {location.name}
                                </option>
                            )
                        })}
                    </>
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )
}