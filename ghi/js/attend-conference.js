window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('conference')
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        console.log(conference)
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }

    selectTag.classList.remove("d-none")

    const loadingTag = document.getElementById('loading-conference-spinner')
    loadingTag.classList.add('d-none')

    const formTag = document.getElementById('create-attendee-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const jsonData = {}
        for (let [key,value] of formData.entries()) {
            jsonData[key] = value;
        }
        const json = JSON.stringify(jsonData)
        console.log(json)
        const attendeeUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newAttendee = await response.json();
            console.log(newAttendee)
            const successTag = document.getElementById('success-message')
            successTag.classList.remove("d-none")
        }
    })
  
  });