window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        console.log(data)
        const selectTag = document.getElementById('location')
        for (let location of data.locations) {
            console.log(location)
            const option = document.createElement('option')
            option.value = location.id
            option.innerHTML = location.name
            selectTag.appendChild(option)
        }
    }

    const formTag = document.getElementById('create-conference-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const jsonData = {}
        for (let [key, value] of formData.entries()) {
            jsonData[key] = value;
        }
        const json = JSON.stringify(jsonData)
        console.log(jsonData)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        console.log(json)
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference)
        }
    });
});