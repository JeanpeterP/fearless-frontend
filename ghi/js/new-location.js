window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/"

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.log("Bad response");
        } else {
            const data = await response.json();
            console.log(data)
            const selectTag = document.getElementById("state")
            for (let state of data.states) {
                const opt = document.createElement('option')
                const stateName = Object.values(state)[0]
                opt.value = state.abbreviation;
                opt.innerHTML = state.name;
                selectTag.appendChild(opt)
            }
        }
    } catch(e) {
        console.log("Error raised", (e))
    }

    const form = document.getElementById('create-location-form');
    form.addEventListener('submit', async event => {
        event.preventDefault();
        console.log('Form submitted');
        const formData = new FormData(form);
        const jsonData = {};
        for (let [key, value] of formData.entries()) {
            jsonData[key] = value;
        };
        const json = JSON.stringify(jsonData)
        //const json = JSON.stringify(Object.fromEntries(formData.entries()));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          form.reset();
          const newLocation = await response.json();
          console.log(newLocation);
        }
    });
});