function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
        <div class="card mb-3 shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle text-muted mb-2">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
               <p>Hello There</p>
               ${new Date(starts).toLocaleDateString()} -
               ${new Date(ends).toLocaleDateString()}
            </div> 
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        
        if (!response.ok) {
            // Do something when the response is bad
            console.log("bad response")
        } else {
            const data = await response.json();
            console.log(data)
            let index = 0;
            for (let conference of data.conferences){
                console.log(conference)
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details)
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = details.conference.starts
                    const ends = details.conference.ends
                    const html = createCard(title, description, pictureUrl, location, starts, ends);
                    const column = document.querySelector(`#col-${index % 3}`);
                    column.innerHTML += html;
                    index += 1;
                }
            }
        }
    } catch(e) {
        // Do something if error is raised
        console.log("error raised", (e))
    }
});
